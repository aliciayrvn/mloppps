res = chain(
    resample_data.s(tuple(series), c=c.model_dump()),
    compute_masks.s(c=c.model_dump()),
    slice_pathline.s(c=c.model_dump(), slice_num=200),
    done.s(c=c.model_dump()),
)()
